<!DOCTYPE html>

<html class="no-js" lang="zxx">
<head>
	
	@include('site/layouts.head', array('title' => 'XFORKS'))

</head>
<body>

<!-- WRAPPER ALL -->
<div class="macro_tm_wrapper_all" data-audio="off">
  

    <!--START TOP AREA-->
	@include('site/layouts.header')

  		@yield('content')

	@yield('styles')
	
	@include('site/layouts.footer')
 @yield('scripts')
</div>

</body>
</html> 