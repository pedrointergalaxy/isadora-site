

<!doctype html>

<html class="no-js" lang="zxx">
<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<meta name="description" content="Olá sou a Isadora Pompeo e esse é o meu site, venha visitar">
<meta name="author" content="marketify">

<meta name="viewport" content="Isadora Pompeo, cantora gospel,">

<title>Isadora Pompeo - Oficial</title>

<link rel="shortcut icon" href="img/favicon.png" />

<!-- STYLES -->
<link href="https://fonts.googleapis.com/css?family=Poppins:300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/plugins.css" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<!--[if lt IE 9]> <script type="text/javascript" src="js/modernizr.custom.js"></script> <![endif]-->
<!-- /STYLES -->

</head>

@yield('styles')

@section('scripts')
 <!--====== SCRIPTS JS ======-->
    <!-- SCRIPTS -->
<script src="js/jquery.js"></script>
<script src="js/plugins.js"></script>
<script src="js/init.js"></script>
<!-- /SCRIPTS -->
@stop