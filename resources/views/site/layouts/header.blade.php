 <!-- HEADER -->
    <header class="macro_tm_header">
        <div class="in">
            <div class="in_relative">
                <div class="logo">
                    <a class="light" href="/"><img src="img/logo/logo-isadorapompeo-red.png" alt="logo" /></a>
                    <a class="dark" href="/"><img src="img/logo/logo-isadorapompeo-red.png" alt="logo" /></a>
                </div>
                <div class="navigation_list_wrap">
                    <div class="menu">
                        <ul class="anchor_nav">
                            <li><a href="#">Home</a></li>
                            <li><a href="#events">Eventos</a></li>
                            <li><a target="_blank"  href="https://open.spotify.com/user/isadorapompeo">Músicas</a></li>
                            <li><a href="#sobre">Biografia</a></li>
                            <li><a href="#contato">Contato</a></li>
                            <li><a href="#shop">Shop</a></li>
                        </ul>
                    </div>
                    <div class="social_icons">
                        <ul>
                            <li class="audio">
                                <a  class="voice_on" href="#"><i class="xcon-volume-up"></i></a>
                            </li>
                            <li class="icon"><a target="_blank"  href="https://www.facebook.com/Isadorapompeo/"><i class="xcon-facebook"></i></a></li>
                            <li class="icon"><a target="_blank"  href="https://twitter.com/isadora_pompeo"><i class="xcon-twitter"></i></a></li>
                            <li class="icon"><a target="_blank"  href="https://www.instagram.com/isadorapompeo/"><i class="xcon-instagram"></i></a></li>
                           <li class="icon"><a target="_blank"  href="https://www.youtube.com/channel/UCAcbgThZJ4Iq-f1oPl4p8jw"><i class="xcon-youtube-play"></i></a></li>
                        </ul>
                    </div>
                    <div class="macro_tm_trigger">
                        <div class="hamburger hamburger--collapse-r">
                            <div class="hamburger-box">
                                <div class="hamburger-inner"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
         <!-- MOBILE MENU -->
        <div class="macro_tm_mobile_menu_wrap">
            <ul class="anchor_nav">
                <li><a href="#">Home</a></li>
                <li><a href="#events">Eventos</a></li>
                <li><a target="_blank" href="https://open.spotify.com/user/isadorapompeo">Músicas</a></li>
                <li><a href="#sobre">Biografia</a></li>
                <li><a href="#contato">Contato</a></li>
                <li><a href="#shop">Shop</a></li>
            </ul>
        </div>
    </header>
    <!-- /HEADER -->
    