@extends('site.layouts.default')

@section('title')
@parent
@stop

@section('content')

<!-- CONTENT -->
    <div class="macro_tm_content">
    
        <!-- HOME -->
        <div class="macro_tm_section" id="home">
            <div class="macro_tm_top_bg_wrap home">
                <div class="top_bg">
                    <div class="overlay_video">
                        <video autoplay loop muted>
                            <source src="video/clipe.mp4" type="video/mp4">
                        </video>
                    </div>
                    <div class="overlay_color video"></div>
                </div>
                <div class="top_bg_content home">
                    <div class="marketify_text_slideshow">
                        <div class="fn_text_slideshow" data-effect="fx9" data-interval="4000"> <!-- fx1...fx18, min 2000... max 10000 -->
                            <!--<div class="slide current"><h2 class="title">Macro</h2></div>
                            <div class="slide"><h2 class="title">Professional</h2></div>
                            <div class="slide"><h2 class="title">Awesome</h2></div>
                            <div class="slide"><h2 class="title">Creative</h2></div>
                            <div class="slide"><h2 class="title">Template</h2></div>-->
                        </div>
                    </div>
                    <div class="down">
                        <div class="in anchor">
                            <a href="#about"><span class="animated"><i class="xcon-angle-down"></i></span></a>
                        </div>
                    </div>
                    <div class="effectiv">
                        <img class="svg" src="img/svg/divider1.svg" alt="" />
                    </div>
                </div>
            </div>
        </div>
        
        <!--<div class="macro_tm_section" id="home">

             <div class="macro_tm_top_bg_wrap home">

                <div id="video" class="center" align="center">
                  <iframe width="1920" height="1080" src="https://www.youtube.com/embed/7lgaJ_DyyPQ?autoplay=1&loop=1&playlist=7lgaJ_DyyPQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>

            </div>

        </div>-->


        <!-- /HOME -->
        
        <!-- ABOUT -->
        <div class="macro_tm_section" id="about">
            <div class="macro_tm_title_holder about">
                <br>
            </div>
            <div class="macro_tm_about_me_wrap">
                <div class="my_image main">
                    <div class="overlay_about"></div>
                </div>
                <div class="image_definition">
                    <div class="name_holder">
                        <h3>Oi! Eu sou a <span style="color: #ff1952;">Isadora Pompeo.</span></h3>
                    </div>
                    <div class="description">
                        <p>Cantora e apaixonada por Jesus!<br> Bem-vindos ao meu site! Aqui você encontra um pouco de quem sou eu e do que estou fazendo.<br> É um prazer tê-lo aqui! </p>
                    </div>
                    <div class="contact_author">
                        <p class="phone"><label>Telefone:</label><span>+55 54 9622-3077</span></p>
                        <p class="mail"><label>Contato:</label>Lucia Martini</p>
                        <label class="social">Redes Sociais:</label>
                        <ul>
                             <li class="icon"><a href="https://www.facebook.com/Isadorapompeo/"><i class="xcon-facebook"></i></a></li>
                            <li class="icon"><a href="https://twitter.com/isadora_pompeo"><i class="xcon-twitter"></i></a></li>
                            <li class="icon"><a href="https://www.instagram.com/isadorapompeo/"><i class="xcon-instagram"></i></a></li>
                           <li class="icon"><a href="https://www.youtube.com/channel/UCAcbgThZJ4Iq-f1oPl4p8jw"><i class="xcon-youtube-play"></i></a></li>
                          
                        </ul>
                    </div>
                    <div class="sign">
                        <img src="img/sign/sign-half-dark-b.png" alt="" />
                    </div>
                </div>
            </div>
        </div>
        
        <!-- SKILLS -->
        <div id="sobre" class="macro_tm_section">
            <div class="macro_tm_skills_wrap">
                <div class="container">
                    <div class="inner">
                        <div class="left">
                            <div class="title_in">
                                <h3>Skills</h3>
                            </div>
                             <div class="macro_tm_progress_wrap" data-size="small" data-round="c" data-strip="off">
                                <div class="macro_tm_progress" data-value="100" data-color="#ff1952">
                                    <span><span class="label">Cantora</span><span class="number">100%</span></span>
                                    <div class="macro_tm_bar_bg"><div class="macro_tm_bar_wrap"><div class="macro_tm_bar"></div></div></div>
                                </div>
                                <div class="macro_tm_progress" data-value="90" data-color="#ff1952">
                                    <span><span class="label">Atriz</span><span class="number">90%</span></span>
                                    <div class="macro_tm_bar_bg"><div class="macro_tm_bar_wrap"><div class="macro_tm_bar"></div></div></div>
                                </div>
                                <div class="macro_tm_progress" data-value="70" data-color="#ff1952">
                                    <span><span class="label">Modelo</span><span class="number">70%</span></span>
                                    <div class="macro_tm_bar_bg"><div class="macro_tm_bar_wrap"><div class="macro_tm_bar"></div></div></div>
                                </div>
                            </div>
                        </div>
                        <div class="right">
                            <div class="title_in">
                                <h3>Sobre mim</h3>
                            </div>
                            <p>Revelada no YouTube, Isadora Pompeo se tornou referência jovem no mundo gospel. Com mais de 2 milhões de inscritos em seu canal, ela ficou conhecida pelas canções Minha Morada e Oi, Jesus. Hoje sua caminhada é influência para vida de milhares de jovens na internet. Compartilhando do seu dia a dia com Jesus, Isadora não é só uma cantora, mas também, uma propagadora da Palavra.</p>

                        </div>
                    </div>
                </div>
            </div>
        </div> 
    </div>
    
        <!-- /SKILLS -->
        
        <!-- TESTIMONIAL -->
        
        <!-- /TESTIMONIAL -->

        <!-- FOTOS -->
        <div class="macro_tm_section" id="portfolio">
            <div class="macro_tm_portfolio_wrap">
                <div class="macro_tm_title_holder portfolio_filter">
                    <span class="title_section">FOTOS</span>

                    <span class="line"></span>
                </div>
                <div class="macro_tm_portfolio_wrap" data-col="4" data-space="10" data-lightbox-gallery="on" data-hover-img="scale" data-overlay-bgcolor="transDark" data-text-hor-pos="center" data-text-ver-pos="middle"  data-hover-animate="zoomIn" data-round="e" data-animation="woop" data-delay="300">
                    <ul class="macro_tm_portfolio_filter">
                        <li><a href="#" class="current" data-filter="*">Todas</a></li>
                       <!--  <li><a href="#" data-filter=".Isadora">Isadora</a></li>
                        <li><a href="#" data-filter=".Shows">Shows</a></li>
                        <li><a href="#" data-filter=".Eventos">Eventos</a></li> -->
                    </ul>
                    <ul class="macro_tm_portfolio_list gallery_zoom">
                        <li class="people">
                            <article class="item_holder">
                                <img class="image" src="img/gallery/isadora/Isadora-1.jpg" alt="" />
                                <div class="overlay">
                                    <div class="clickable_wrap">
                                        <a class="zoom" href="img/gallery/isadora/Isadora-1.jpg"><img class="svg" src="img/svg/zoom.svg" alt="" />
                                            <span class="definition_icon zoom">Zoom</span>
                                        </a>
                                       
                                    </div>
                                    <div class="portfolio_title">
                                        <h5 class="title_name"><a href="portfolio_single.html">Foto1</a></h5>
                                    </div>
                                </div>
                            </article>
                        </li>
                        <li class="nature">
                            <article class="item_holder">
                                <img class="image" src="img/gallery/isadora/Isadora-2.jpg" alt="" />
                                <div class="overlay">
                                    <div class="clickable_wrap">
                                        <a class="zoom" href="img/gallery/isadora/Isadora-2.jpg"><img class="svg" src="img/svg/zoom.svg" alt="" />
                                            <span class="definition_icon zoom">Zoom</span>
                                        </a>
                                       
                                    </div>
                                    <div class="portfolio_title">
                                        <h5 class="title_name"><a href="portfolio_single.html">Foto 2</a></h5>
                                    </div>
                                </div>
                            </article>
                        </li>
                        <li class="animal">
                            <article class="item_holder">
                                <img class="image" src="img/gallery/isadora/Isadora-3.jpg" alt="" />
                                <div class="overlay">
                                    <div class="clickable_wrap">
                                        <a class="zoom" href="img/gallery/isadora/Isadora-3.jpg"><img class="svg" src="img/svg/zoom.svg" alt="" />
                                            <span class="definition_icon zoom">Zoom</span>
                                        </a>
                                       
                                    </div>
                                    <div class="portfolio_title">
                                        <h5 class="title_name"><a href="portfolio_single.html">Foto 3</a></h5>
                                    </div>
                                </div>
                            </article>
                        </li>
                        <li class="animal" >
                            <article class="item_holder">
                                <img class="image" src="img/gallery/isadora/Isadora-4.jpg" alt="" />
                                <div class="overlay">
                                    <div class="clickable_wrap">
                                        <a class="zoom" href="img/gallery/isadora/Isadora-4.jpg"><img class="svg" src="img/svg/zoom.svg" alt="" />
                                            <img class="svg" src="img/svg/play.svg" alt="" />
                                            <span class="definition_icon play">Play</span>
                                        </a>
                                       
                                    </div>
                                    <div class="portfolio_title">
                                        <h5 class="title_name"><a href="portfolio_single.html">Foto 4</a></h5>
                                    </div>
                                </div>
                            </article>
                        </li>
                        <li class="animal">
                            <article class="item_holder">
                                <img class="image" src="img/gallery/isadora/Isadora-5.jpg" alt="" />
                                <div class="overlay">
                                    <div class="clickable_wrap">
                                        <a class="zoom" href="img/gallery/isadora/Isadora-5.jpg"><img class="svg" src="img/svg/zoom.svg" alt="" />
                                            <span class="definition_icon zoom">Zoom</span>
                                        </a>
                                       
                                    </div>
                                    <div class="portfolio_title">
                                        <h5 class="title_name"><a href="portfolio_single.html">Foto 5</a></h5>
                                    </div>
                                </div>
                            </article>
                        </li>
                        <li class="animal">
                            <article class="item_holder">
                                <img class="image" src="img/gallery/isadora/Isadora-6.jpg" alt="" />
                                <div class="overlay">
                                    <div class="clickable_wrap">
                                        <a class="zoom" href="img/gallery/isadora/Isadora-6.jpg"><img class="svg" src="img/svg/zoom.svg" alt="" />
                                            <span class="definition_icon zoom">Zoom</span>
                                        </a>
                                       
                                    </div>
                                    <div class="portfolio_title">
                                        <h5 class="title_name"><a href="portfolio_single.html">Foto 6</a></h5>
                                    </div>
                                </div>
                            </article>
                        </li>
                        <li class="nature">
                            <article class="item_holder">
                                <img class="image" src="img/gallery/isadora/Isadora-7.jpg" alt="" />
                                <div class="overlay">
                                    <div class="clickable_wrap">
                                        <a class="zoom" href="img/gallery/isadora/Isadora-7.jpg"><img class="svg" src="img/svg/zoom.svg" alt="" />
                                            <span class="definition_icon zoom">Zoom</span>
                                        </a>
                                       
                                    </div>
                                    <div class="portfolio_title">
                                        <h5 class="title_name"><a href="portfolio_single.html">Foto 7</a></h5>
                                    </div>
                                </div>
                            </article>
                        </li>
                        <li class="people">
                            <article class="item_holder">
                                <img class="image" src="img/gallery/isadora/Isadora-8.jpg" alt="" />
                                <div class="overlay">
                                    <div class="clickable_wrap">
                                        <a class="zoom" href="img/gallery/isadora/Isadora-8.jpg"><img class="svg" src="img/svg/zoom.svg" alt="" />
                                            <span class="definition_icon zoom">Zoom</span>
                                        </a>
                                       
                                    </div>
                                    <div class="portfolio_title">
                                        <h5 class="title_name"><a href="portfolio_single.html">Foto 8</a></h5>
                                    </div>
                                </div>
                            </article>
                        </li>
                        <li class="people">
                            <article class="item_holder">
                                <img class="image" src="img/gallery/isadora/Isadora-9.jpg" alt="" />
                                <div class="overlay">
                                    <div class="clickable_wrap">
                                        <a class="zoom" href="img/gallery/isadora/Isadora-9.jpg"><img class="svg" src="img/svg/zoom.svg" alt="" />
                                            <span class="definition_icon zoom">Zoom</span>
                                        </a>
                                       
                                    </div>
                                    <div class="portfolio_title">
                                        <h5 class="title_name"><a href="portfolio_single.html">Foto 9</a></h5>
                                    </div>
                                </div>
                            </article>
                        </li>
                        <li class="people">
                            <article class="item_holder">
                                <img class="image" src="img/gallery/isadora/Isadora-10.jpg" alt="" />
                                <div class="overlay">
                                    <div class="clickable_wrap">
                                        <a class="zoom" href="img/gallery/isadora/Isadora-10.jpg"><img class="svg" src="img/svg/zoom.svg" alt="" />
                                            <span class="definition_icon zoom">Zoom</span>
                                        </a>
                                       
                                    </div>
                                    <div class="portfolio_title">
                                        <h5 class="title_name"><a href="portfolio_single.html">Foto 10</a></h5>
                                    </div>
                                </div>
                            </article>
                        </li>
                        <li class="people">
                            <article class="item_holder">
                                <img class="image" src="img/gallery/isadora/Isadora-11.jpg" alt="" />
                                <div class="overlay">
                                    <div class="clickable_wrap">
                                        <a class="zoom" href="img/gallery/isadora/Isadora-11.jpg"><img class="svg" src="img/svg/zoom.svg" alt="" />
                                            <span class="definition_icon zoom">Zoom</span>
                                        </a>
                                       
                                    </div>
                                    <div class="portfolio_title">
                                        <h5 class="title_name"><a href="portfolio_single.html">Foto 11</a></h5>
                                    </div>
                                </div>
                            </article>
                        </li>
                        <li class="people">
                            <article class="item_holder">
                                <img class="image" src="img/gallery/isadora/Isadora-12.jpg" alt="" />
                                <div class="overlay">
                                    <div class="clickable_wrap">
                                        <a class="zoom" href="img/gallery/isadora/Isadora-12.jpg"><img class="svg" src="img/svg/zoom.svg" alt="" />
                                            <span class="definition_icon zoom">Zoom</span>
                                        </a>
                                       
                                    </div>
                                    <div class="portfolio_title">
                                        <h5 class="title_name"><a href="portfolio_single.html">Foto 12</a></h5>
                                    </div>
                                </div>
                            </article>
                        </li>
                        <li class="people">
                            <article class="item_holder">
                                <img class="image" src="img/gallery/isadora/Isadora-13.jpg" alt="" />
                                <div class="overlay">
                                    <div class="clickable_wrap">
                                        <a class="zoom" href="img/gallery/isadora/Isadora-13.jpg"><img class="svg" src="img/svg/zoom.svg" alt="" />
                                            <span class="definition_icon zoom">Zoom</span>
                                        </a>
                                       
                                    </div>
                                    <div class="portfolio_title">
                                        <h5 class="title_name"><a href="portfolio_single.html">Foto 13</a></h5>
                                    </div>
                                </div>
                            </article>
                        </li>
                        <li class="people">
                            <article class="item_holder">
                                <img class="image" src="img/gallery/isadora/Isadora-14.jpg" alt="" />
                                <div class="overlay">
                                    <div class="clickable_wrap">
                                        <a class="zoom" href="img/gallery/isadora/Isadora-14.jpg"><img class="svg" src="img/svg/zoom.svg" alt="" />
                                            <span class="definition_icon zoom">Zoom</span>
                                        </a>
                                       
                                    </div>
                                    <div class="portfolio_title">
                                        <h5 class="title_name"><a href="portfolio_single.html">Foto 14</a></h5>
                                    </div>
                                </div>
                            </article>
                        </li>
                        <li class="people">
                            <article class="item_holder">
                                <img class="image" src="img/gallery/isadora/Isadora-15.jpg" alt="" />
                                <div class="overlay">
                                    <div class="clickable_wrap">
                                        <a class="zoom" href="img/gallery/isadora/Isadora-15.jpg"><img class="svg" src="img/svg/zoom.svg" alt="" />
                                            <span class="definition_icon zoom">Zoom</span>
                                        </a>
                                       
                                    </div>
                                    <div class="portfolio_title">
                                        <h5 class="title_name"><a href="portfolio_single.html">Foto 15</a></h5>
                                    </div>
                                </div>
                            </article>
                        </li>
                        <li class="people">
                            <article class="item_holder">
                                <img class="image" src="img/gallery/isadora/Isadora-16.jpg" alt="" />
                                <div class="overlay">
                                    <div class="clickable_wrap">
                                        <a class="zoom" href="img/gallery/isadora/Isadora-16.jpg"><img class="svg" src="img/svg/zoom.svg" alt="" />
                                            <span class="definition_icon zoom">Zoom</span>
                                        </a>
                                       
                                    </div>
                                    <div class="portfolio_title">
                                        <h5 class="title_name"><a href="portfolio_single.html">Foto 16</a></h5>
                                    </div>
                                </div>
                            </article>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- /FOTOS -->



        <!-- SHOP -->
        <div id="shop" class="macro_tm_section">
            <div class="macro_tm_team_member_wrap">
                <div class="macro_tm_list_wrap section_team" data-column="3" data-space="20">
                <div class="macro_tm_title_holder team">
                    <span class="title_section">Shop</span>
                    <span class="line"></span>
                </div>
                <div class="container">
                    <ul class="macro_list owl-carousel">
                        <li class="list_li item">
                            <div class="list_in">
                                <div class="inner">
                                    <div class="list_image">
                                        <a href="#"><img src="img/shop/bone1.jpg" alt="" /></a>
                                    </div>
                                    <div class="overlay_image"><a href="#"></a></div>
                                </div>
                                <div class="macro_tm_team_author_definition">
                                    <div class="team_information">
                                        <span class="name"><a href="#">Boné 1</a></span>
                                        <span class="job">R$ <strong>50</strong></span>
                                    </div>
                                    <div class="macro_tm_purchase_button">
                                            <div class="in">
                                                <a href="#"><i class="xcon-basket"></i>Comprar</a>
                                            </div>
                                    </div>
                                    <div class="social_icons">
                                        <ul>
                                              <li class="icon"><a href="https://www.facebook.com/Isadorapompeo/"><i class="xcon-facebook"></i></a></li>
                            <li class="icon"><a href="https://twitter.com/isadora_pompeo"><i class="xcon-twitter"></i></a></li>
                            <li class="icon"><a href="https://www.instagram.com/isadorapompeo/"><i class="xcon-instagram"></i></a></li>
                           <li class="icon"><a href="https://www.youtube.com/channel/UCAcbgThZJ4Iq-f1oPl4p8jw"><i class="xcon-youtube-play"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="list_li item">
                            <div class="list_in">
                                <div class="inner">
                                    <div class="list_image">
                                        <a href="#"><img src="img/shop/camiseta1.jpg" alt="" /></a>
                                    </div>
                                    <div class="overlay_image"><a href="#"></a></div>
                                </div>
                                <div class="macro_tm_team_author_definition">
                                    <div class="team_information">
                                        <span class="name"><a href="#">Camiseta 1</a></span>
                                        <span class="job">R$ <strong>50</strong></span>
                                    </div>
                                    <div class="macro_tm_purchase_button">
                                            <div class="in">
                                                <a href="#"><i class="xcon-basket"></i>Comprar</a>
                                            </div>
                                    </div>
                                    <div class="social_icons">
                                        <ul>
                                            <li class="icon"><a href="https://www.facebook.com/Isadorapompeo/"><i class="xcon-facebook"></i></a></li>
                            <li class="icon"><a href="https://twitter.com/isadora_pompeo"><i class="xcon-twitter"></i></a></li>
                            <li class="icon"><a href="https://www.instagram.com/isadorapompeo/"><i class="xcon-instagram"></i></a></li>
                           <li class="icon"><a href="https://www.youtube.com/channel/UCAcbgThZJ4Iq-f1oPl4p8jw"><i class="xcon-youtube-play"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="list_li item">
                            <div class="list_in">
                                <div class="inner">
                                    <div class="list_image">
                                        <a href="#"><img src="img/shop/camiseta2.jpg" alt="" /></a>
                                    </div>
                                    <div class="overlay_image"><a href="#"></a></div>
                                </div>
                                <div class="macro_tm_team_author_definition">
                                    <div class="team_information">
                                        <span class="name"><a href="#">Camiseta 2</a></span>
                                        <span class="job">R$ <strong>50</strong></span>
                                    </div>
                                    <div class="macro_tm_purchase_button">
                                            <div class="in">
                                                <a href="#"><i class="xcon-basket"></i>Comprar</a>
                                            </div>
                                    </div>
                                    <div class="social_icons">
                                        <ul>
                                            <li class="icon"><a href="https://www.facebook.com/Isadorapompeo/"><i class="xcon-facebook"></i></a></li>
                            <li class="icon"><a href="https://twitter.com/isadora_pompeo"><i class="xcon-twitter"></i></a></li>
                            <li class="icon"><a href="https://www.instagram.com/isadorapompeo/"><i class="xcon-instagram"></i></a></li>
                           <li class="icon"><a href="https://www.youtube.com/channel/UCAcbgThZJ4Iq-f1oPl4p8jw"><i class="xcon-youtube-play"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="list_li item">
                            <div class="list_in">
                                <div class="inner">
                                    <div class="list_image">
                                        <a href="#"><img src="img/shop/camiseta3.jpg" alt="" /></a>
                                    </div>
                                    <div class="overlay_image"><a href="#"></a></div>
                                </div>
                                <div class="macro_tm_team_author_definition">
                                    <div class="team_information">
                                        <span class="name"><a href="#">Camiseta 3</a></span>
                                        <span class="job">R$ <strong>50</strong></span>
                                    </div>
                                    <div class="macro_tm_purchase_button">
                                            <div class="in">
                                                <a href="#"><i class="xcon-basket"></i>Comprar</a>
                                            </div>
                                    </div>
                                    <div class="social_icons">
                                        <ul>
                                            <li class="icon"><a href="https://www.facebook.com/Isadorapompeo/"><i class="xcon-facebook"></i></a></li>
                            <li class="icon"><a href="https://twitter.com/isadora_pompeo"><i class="xcon-twitter"></i></a></li>
                            <li class="icon"><a href="https://www.instagram.com/isadorapompeo/"><i class="xcon-instagram"></i></a></li>
                           <li class="icon"><a href="https://www.youtube.com/channel/UCAcbgThZJ4Iq-f1oPl4p8jw"><i class="xcon-youtube-play"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="list_li item">
                            <div class="list_in">
                                <div class="inner">
                                    <div class="list_image">
                                        <a href="#"><img src="img/shop/bone2.jpg" alt="" /></a>
                                    </div>
                                    <div class="overlay_image"><a href="#"></a></div>
                                </div>
                                <div class="macro_tm_team_author_definition">
                                    <div class="team_information">
                                        <span class="name"><a href="#">Boné 2</a></span>
                                        <span class="job">R$ <strong>50</strong></span>
                                    </div>
                                    <div class="macro_tm_purchase_button">
                                            <div class="in">
                                                <a href="#"><i class="xcon-basket"></i>Comprar</a>
                                            </div>
                                    </div>
                                    <div class="social_icons">
                                        <ul>
                                              <li class="icon"><a href="https://www.facebook.com/Isadorapompeo/"><i class="xcon-facebook"></i></a></li>
                            <li class="icon"><a href="https://twitter.com/isadora_pompeo"><i class="xcon-twitter"></i></a></li>
                            <li class="icon"><a href="https://www.instagram.com/isadorapompeo/"><i class="xcon-instagram"></i></a></li>
                           <li class="icon"><a href="https://www.youtube.com/channel/UCAcbgThZJ4Iq-f1oPl4p8jw"><i class="xcon-youtube-play"></i></a></li>s
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="list_li item">
                            <div class="list_in">
                                <div class="inner">
                                    <div class="list_image">
                                        <a href="#"><img src="img/shop/camiseta4.jpg" alt="" /></a>
                                    </div>
                                    <div class="overlay_image"><a href="#"></a></div>
                                </div>
                                <div class="macro_tm_team_author_definition">
                                    <div class="team_information">
                                        <span class="name"><a href="#">Camiseta 4</a></span>
                                        <span class="job">R$ <strong>50</strong></span>
                                    </div>
                                    <div class="macro_tm_purchase_button">
                                            <div class="in">
                                                <a href="#"><i class="xcon-basket"></i>Comprar</a>
                                            </div>
                                    </div>
                                    <div class="social_icons">
                                        <ul>
                                            <li class="icon"><a href="https://www.facebook.com/Isadorapompeo/"><i class="xcon-facebook"></i></a></li>
                            <li class="icon"><a href="https://twitter.com/isadora_pompeo"><i class="xcon-twitter"></i></a></li>
                            <li class="icon"><a href="https://www.instagram.com/isadorapompeo/"><i class="xcon-instagram"></i></a></li>
                           <li class="icon"><a href="https://www.youtube.com/channel/UCAcbgThZJ4Iq-f1oPl4p8jw"><i class="xcon-youtube-play"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </li>
                         <li class="list_li item">
                            <div class="list_in">
                                <div class="inner">
                                    <div class="list_image">
                                        <a href="#"><img src="img/shop/camiseta5.jpg" alt="" /></a>
                                    </div>
                                    <div class="overlay_image"><a href="#"></a></div>
                                </div>
                                <div class="macro_tm_team_author_definition">
                                    <div class="team_information">
                                        <span class="name"><a href="#">Camiseta 5</a></span>
                                        <span class="job">R$ <strong>50</strong></span>
                                    </div>
                                    <div class="macro_tm_purchase_button">
                                            <div class="in">
                                                <a href="#"><i class="xcon-basket"></i>Comprar</a>
                                            </div>
                                    </div>
                                    <div class="social_icons">
                                        <ul>
                                            <li class="icon"><a href="https://www.facebook.com/Isadorapompeo/"><i class="xcon-facebook"></i></a></li>
                            <li class="icon"><a href="https://twitter.com/isadora_pompeo"><i class="xcon-twitter"></i></a></li>
                            <li class="icon"><a href="https://www.instagram.com/isadorapompeo/"><i class="xcon-instagram"></i></a></li>
                           <li class="icon"><a href="https://www.youtube.com/channel/UCAcbgThZJ4Iq-f1oPl4p8jw"><i class="xcon-youtube-play"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </li>
                         <li class="list_li item">
                            <div class="list_in">
                                <div class="inner">
                                    <div class="list_image">
                                        <a href="#"><img src="img/shop/camiseta6.jpg" alt="" /></a>
                                    </div>
                                    <div class="overlay_image"><a href="#"></a></div>
                                </div>
                                <div class="macro_tm_team_author_definition">
                                    <div class="team_information">
                                        <span class="name"><a href="#">Camiseta 6</a></span>
                                        <span class="job">R$ <strong>50</strong></span>
                                    </div>
                                    <div class="macro_tm_purchase_button">
                                            <div class="in">
                                                <a href="#"><i class="xcon-basket"></i>Comprar</a>
                                            </div>
                                    </div>
                                    <div class="social_icons">
                                        <ul>
                                            <li class="icon"><a href="https://www.facebook.com/Isadorapompeo/"><i class="xcon-facebook"></i></a></li>
                            <li class="icon"><a href="https://twitter.com/isadora_pompeo"><i class="xcon-twitter"></i></a></li>
                            <li class="icon"><a href="https://www.instagram.com/isadorapompeo/"><i class="xcon-instagram"></i></a></li>
                           <li class="icon"><a href="https://www.youtube.com/channel/UCAcbgThZJ4Iq-f1oPl4p8jw"><i class="xcon-youtube-play"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="list_li item">
                            <div class="list_in">
                                <div class="inner">
                                    <div class="list_image">
                                        <a href="#"><img src="img/shop/bone3.jpg" alt="" /></a>
                                    </div>
                                    <div class="overlay_image"><a href="#"></a></div>
                                </div>
                                <div class="macro_tm_team_author_definition">
                                    <div class="team_information">
                                        <span class="name"><a href="#">Boné 3</a></span>
                                        <span class="job"><strong>R$ 50</strong></span>
                                    </div>
                                    <div class="macro_tm_purchase_button">
                                            <div class="in">
                                                <a href="#"><i class="xcon-basket"></i>Comprar</a>
                                            </div>
                                    </div>
                                    <div class="social_icons">
                                        <ul>
                                            <li class="icon"><a href="https://www.facebook.com/Isadorapompeo/"><i class="xcon-facebook"></i></a></li>
                            <li class="icon"><a href="https://twitter.com/isadora_pompeo"><i class="xcon-twitter"></i></a></li>
                            <li class="icon"><a href="https://www.instagram.com/isadorapompeo/"><i class="xcon-instagram"></i></a></li>
                           <li class="icon"><a href="https://www.youtube.com/channel/UCAcbgThZJ4Iq-f1oPl4p8jw"><i class="xcon-youtube-play"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            </div>
        </div>
        <!-- /SHOP -->

        
        <!-- COUNTER -->
        <div class="macro_tm_section" id="services">
            <div class="macro_tm_title_holder services">
                <br>
            </div>
            <div class="macro_tm_counter_wrapper_all">
                <div class="macro_tm_jarallax_inner_bg">
                    <div class="overlay_image jarallax" data-speed=".22"></div>
                    <div class="overlay_color"></div>
                </div>
                <div class="macro_tm_jarallax_inner_content">
                    <div class="container">
                        <div class="macro_tm_counter_wrap" data-col="4" data-delay="300">
                            <ul class="macro_tm_counter_list">
                                <li>
                                    <h3 class="macro_tm_counter" data-from="0" data-to="150000000" data-speed="1500">0</h3>
                                    <span>Seguidores</span>
                                </li>
                                <li>
                                    <h3 class="macro_tm_counter" data-from="0" data-to="230" data-speed="3000">0</h3>
                                    <span>Shows</span>
                                </li>
                                <li>
                                    <h3 class="macro_tm_counter" data-from="0" data-to="200000" data-speed="3000">0</h3>
                                    <span>Ingressos Vendidos</span>
                                </li>
                                <li>
                                    <h3 class="macro_tm_counter" data-from="0" data-to="1000000" data-speed="3000">0</h3>
                                    <span>CDs vendidos</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /COUNTER -->
        
        <!-- EVENTOS -->
        <div class="macro_tm_section" id="events">
                    <div class="macro_tm_blog_wrap">
                        <div class="macro_tm_title_holder blog">
                        <span class="title_section">PRÓXIMOS EVENTOS</span>
                        <span class="line"></span>
                    </div>
                        <div class="macro_tm_list_wrap section_blog" data-column="1" data-space="10">
                            <div class="container">
                                <table class="lista_eventos">
                                      <tr>
                                        <th>DATA</th>
                                        <th>LOCAL</th>
                                        <th>NOME</th>
                                      </tr>
                                      <tr>
                                        <td><a href="#">19 de ABRIL - 2019</a></td>
                                        <td><a href="#">JARAGUÁ DO SUL, SC</a></td>
                                        <td><a href="#">CONFERÊNCIA OI JESUS</a></td>
                                      </tr>
                                      <tr>
                                        <td><a href="#">20 de ABRIL - 2019</a></td>
                                        <td><a href="#">CHAPECÓ, SC</a></td>
                                        <td><a href="#">ISADORA POMPEO</a></td>
                                      </tr>
                                      <tr>
                                        <td><a href="#">21 de ABRIL - 2019</a></td>
                                        <td><a href="#">VIDEIRA, SC</a></td>
                                        <td><a href="#">CONFERÊNCIA OI JESUS</a></td>
                                      </tr>
                                      <tr>
                                        <td><a href="#">25 de ABRIL - 2019</a></td>
                                        <td><a href="#">ASSIS, SP</a></td>
                                        <td><a href="#">CONFERÊNCIA OI JESUS</a></td>
                                      </tr>
                                      <tr>
                                        <td><a href="#">26 de ABRIL - 2019</a></td>
                                        <td><a href="#">SÃO JOSÉ DO RIO PRETO, SP</a></td>
                                        <td><a href="#">CONFERÊNCIA OI JESUS</a></td>
                                      </tr>
                                      <tr>
                                        <td><a href="#">27 de ABRIL - 2019</a></td>
                                        <td><a href="#">MARÍLIA, SP</a></td>
                                        <td><a href="#">CONFERÊNCIA OI JESUS</a></td>
                                      </tr>
                                      <tr>
                                        <td><a href="#">01 de MAIO - 2019</a></td>
                                        <td><a href="#">GOIANIA, GO</a></td>
                                        <td><a href="#">MARCHA PARA JESUS</a></td>
                                      </tr>
                                      <tr>
                                        <td><a href="#">02 de MAIO - 2019</a></td>
                                        <td><a href="#">UBERLÂNDIA, MG</a></td>
                                        <td><a href="#">CONFERÊNCIA OI JESUS</a></td>
                                      </tr>
                                      <tr>
                                        <td><a href="#">03 de MAIO - 2019</a></td>
                                        <td><a href="#">ARAXÁ, MG</a></td>
                                        <td><a href="#">CONFERÊNCIA OI JESUS</a></td>
                                      </tr>
                                      <tr>
                                        <td><a href="#">04 de MAIO - 2019</a></td>
                                        <td><a href="#">RIO PRETO , SP</a></td>
                                        <td><a href="#">CONFERÊNCIA OI JESUS</a></td>
                                      </tr>
                                      <tr>
                                        <td><a href="#">08 de MAIO - 2019</a></td>
                                        <td><a href="#">APARECIDA DE GOIÂNIA, GO</a></td>
                                        <td><a href="#">APARECIDA É SHOW</a></td>
                                      </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
        <!-- /EVENTOS -->

        <!-- BLOG -->
        <div class="macro_tm_section" id="blog">
            <div class="macro_tm_blog_wrap">
                <div class="macro_tm_title_holder blog">
                <span class="title_section">Blog</span>
                <span class="title_definition">Confira as últimas notícias e novidades da Isa.</span>
                <span class="line"></span>
            </div>
                <div class="macro_tm_list_wrap section_blog" data-column="3" data-space="20">
                    <div class="container">
                        <ul class="macro_list another_animation">
                            <li class="list_li hideforanimation">
                                <div class="list_in">
                                    <a href="#"><img class="image" src="img/blog/1.jpg" alt="" /></a>
                                    <a href="blog_single.html"><div class="overlay_blog"></div></a>
                                    <div class="blog_definitions_wrap">
                                        <p class="date">07 January 2017 / By <a href="#">EQUIPE ISA</a></p>
                                        <span class="title_item"><a href="blog_single.html">Isadora Pompeo faz multidão lotar auditório Canaã em Manaus.</a></span>
                                        <p class="description_blog">Lorem Ipsum is simply dummy text of the printing and typesetting.</p>
                                    </div>
                                    <div class="category_blog">
                                        <span>Categoria A</span>
                                    </div>
                                </div>
                            </li>
                            <li class="list_li hideforanimation">
                                <div class="list_in">
                                    <a href="#"><img class="image" src="img/blog/2.jpg" alt="" /></a>
                                    <a href="blog_single.html"><div class="overlay_blog"></div></a>
                                    <div class="blog_definitions_wrap">
                                        <p class="date">11 February 2017 / By <a href="#">EQUIPE ISA</a></p>
                                        <span class="title_item"><a href="blog_single.html">Confira a nova versão de “Oi, Jesus” em vídeo inédito.</a></span>
                                        <p class="description_blog">Lorem Ipsum is simply dummy text of the printing and typesetting.</p>
                                    </div>
                                    <div class="category_blog">
                                        <span>Categoria B</span>
                                    </div>
                                </div>
                            </li>
                            <li class="list_li hideforanimation">
                                <div class="list_in">
                                    <a href="#"><img class="image" src="img/blog/3.jpg" alt="" /></a>
                                    <a href="blog_single.html"><div class="overlay_blog"></div></a>
                                    <div class="blog_definitions_wrap">
                                        <p class="date">15 March 2017 / By <a href="#">EQUIPE ISA</a></p>
                                        <span class="title_item"><a href="blog_single.html">UMCG prepara novidades para a Expo Cristã 2018.</a></span>
                                        <p class="description_blog">Lorem Ipsum is simply dummy text of the printing and typesetting.</p>
                                    </div>
                                    <div class="category_blog">
                                        <span>Categoria C</span>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- /BLOG -->

        <!-- PARTNER -->
        <div class="macro_tm_title_holder partner">
            <span class="title_section">Parceiros</span>
            <span class="line"></span>
        </div>
        <div class="macro_tm_section">
            <div class="macro_tm_parners_wrap">
                <div class="macro_tm_list_wrap section_partners" data-column="4" data-space="0">
                    <div class="container">
                        <ul class="macro_list owl-carousel">
                            <li class="list_li item">
                                <div class="list_in">
                                    <img src="img/partners/1.png" alt="#" />
                                </div>
                            </li>
                            <li class="list_li item">
                                <div class="list_in">
                                    <img src="img/partners/2.png" alt="#" />
                                </div>
                            </li>
                            <li class="list_li item">
                                <div class="list_in">
                                    <img src="img/partners/3.png" alt="#" />
                                </div>
                            </li>
                            <li class="list_li item">
                                <div class="list_in">
                                    <img src="img/partners/4.png" alt="#" />
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- /PARTNER -->
        
        
        <!-- CONTACT -->
        <div class="macro_tm_section" id="contato">
            <div class="macro_tm_contact_wrap">
                <div class="macro_tm_title_holder contact">
                    <span class="title_section">Contato</span>
                    <span class="title_definition">Para entrar em contato comigo e com a minha equipe</span>
                    <span class="line"></span>
                </div>
                <div class="container">
                    <div class="in">
                        <div class="left">
                            <div class="general">
                                <span class="title">EQUIPE ISA POMPEO</span>
                                <span class="line"></span>
                                <ul>
                                    <li><a>Lucia Martini</a></li>
                                    <li><i class="xcon-phone"></i><span>+55 54 9622-3077</span></li>
                                    
                                </ul>
                            </div>
                            
                        </div>
                        <div class="right">
                            <div class="general">
                                <span class="title">Entre em Contato</span>
                                <span class="line"></span>
                            </div>
                            <div class="contact">
                                <form action="/" method="post" class="contact_form" id="contact_form">
                                    <div class="returnmessage" data-success="Your message has been received, We will contact you soon."></div>
                                    <div class="empty_notice"><span>Preencha os campos a seguir.</span></div>
                                    <div class="input_wrap">
                                        <ul>
                                            <li>
                                                <div class="inner">
                                                    <input id="name" type="text" placeholder="Nome Completo" /> 
                                                </div>
                                            </li>
                                            <li>
                                                <div class="inner">
                                                    <input id="email" type="text" placeholder="E-mail" /> 
                                                </div>
                                            </li>
                                            <li>
                                                <div class="inner">
                                                    <input id="subject" type="text" placeholder="Assunto" /> 
                                                </div>
                                            </li>
                                            <li>
                                                <div class="inner">
                                                    <textarea id="message" placeholder="Mensagem"></textarea> 
                                                </div>
                                            </li>
                                        </ul>
                                        <a id="send_message" class="send_message" href="#"><i class="xcon-direction"></i>Enviar</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /CONTACT -->
    </div>
    <!-- /CONTENT -->


@endsection
